#pragma once

#include "arena_allocator.h"

#include <atomic>
#include <limits>

template <typename T>
struct ElementTraits {
    static T Min() {
        return std::numeric_limits<T>::min();
    }
    static T Max() {
        return std::numeric_limits<T>::max();
    }
};

class SpinLock {
private:
    std::atomic<bool> locked_;
public:
    explicit SpinLock() {
        locked_.store(false);
    }

    void Lock() {
        while(locked_.exchange(true)) {
            // wait
        }
    }

    void Unlock() {
        locked_.store(false);
    }

    // adapters for BasicLockable concept

    void lock() {
        Lock();
    }

    void unlock() {
        Unlock();
    }
};

template <typename T>
class OptimisticLinkedSet {
private:
    struct Node {
        T element_;
        std::atomic<Node*> next_;
        SpinLock lock_{};
        std::atomic<bool> marked_{false};

        Node(const T& element, Node* next = nullptr)
                : element_(element),
                  next_(next) {
        }
    };

    struct Edge {
        Node* pred_;
        Node* curr_;

        Edge(Node* pred, Node* curr)
                : pred_(pred),
                  curr_(curr) {
        }
    };

public:
    explicit OptimisticLinkedSet(ArenaAllocator& allocator)
            : allocator_(allocator) {
        CreateEmptyList();
    }

    bool Insert(const T& element) {
        while (true) {
            Edge edge = Locate(element);
            Node* pred = edge.pred_;
            Node* curr = edge.curr_;

            pred->lock_.lock();
            curr->lock_.lock();

            if (Validate(edge)) {
                if (curr->element_ == element) {
                    curr->lock_.unlock();
                    pred->lock_.unlock();
                    return false;
                }
                else {
                    Node* new_node = allocator_.New<Node>(element);
                    new_node->next_.store(curr);
                    pred->next_.store(new_node);
                    size_.fetch_add(1);
                    curr->lock_.unlock();
                    pred->lock_.unlock();
                    return true;
                }
            }

            curr->lock_.unlock();
            pred->lock_.unlock();
        }
    }

    bool Remove(const T& element) {
        while (true) {
            Edge edge = Locate(element);
            Node* pred = edge.pred_;
            Node* curr = edge.curr_;

            pred->lock_.lock();
            curr->lock_.lock();

            if (Validate(edge)) {
                if (curr->element_ != element) {
                    curr->lock_.unlock();
                    pred->lock_.unlock();
                    return false;
                }
                else {
                    curr->marked_.store(true);
                    pred->next_.store(curr->next_.load());
                    size_.fetch_sub(1);
                    curr->lock_.unlock();
                    pred->lock_.unlock();
                    return true;
                }
            }

            curr->lock_.unlock();
            pred->lock_.unlock();
        }
    }

    bool Contains(const T& element) const {
        Node* curr = this->head_;
        while (curr->element_ < element) {
            curr = curr->next_.load();
        }
        return curr->element_ == element && !curr->marked_.load();
    }

    size_t Size() const {
        return size_.load();
    }

private:
    void CreateEmptyList() {
        head_ = allocator_.New<Node>(ElementTraits<T>::Min());
        head_->next_ = allocator_.New<Node>(ElementTraits<T>::Max());
        size_.store(0);
    }

    Edge Locate(const T& element) const {
        Node* pred = this->head_;
        Node* curr = head_->next_.load();
        while (curr->element_ < element) {
            pred = curr;
            curr = curr->next_.load();
        }
        return Edge{pred, curr};
    }

    bool Validate(const Edge& edge) const {
        return !edge.pred_->marked_.load() &&
               !edge.curr_->marked_.load() &&
               edge.pred_->next_.load() == edge.curr_;
    }

private:
    ArenaAllocator& allocator_;
    Node* head_{nullptr};
    std::atomic<int> size_;
};

template <typename T> using ConcurrentSet = OptimisticLinkedSet<T>;
