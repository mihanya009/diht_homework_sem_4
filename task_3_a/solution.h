#pragma once

#include <deque>
#include <condition_variable>

template <class T, class Container = std::deque<T>>
class BlockingQueue {
    Container container_;
    std::condition_variable put_condition_;
    std::condition_variable get_condition_;
    std::mutex mutex_;
    std::size_t capacity_;
    bool on_;

public:
    explicit BlockingQueue(const std::size_t& capacity) {
        capacity_ = capacity;
        on_ = true;
    }
    void Put(T&& element) {
        std::unique_lock<std::mutex> lock(mutex_);
        if (!on_) {
            throw std::bad_exception();
        }
        put_condition_.wait(lock, [this]() { return !on_ || capacity_ != 0;});
        if (!on_) {
            throw std::bad_exception();
        }
        capacity_--;
        container_.push_back(std::move(element));
        get_condition_.notify_one();
    }
    bool Get(T& result) {
        std::unique_lock<std::mutex> lock(mutex_);
        get_condition_.wait(lock, [this]() { return !on_ || !container_.empty();});
        if (container_.empty()) {
            return false;
        }
        capacity_++;
        result = std::move(container_.front());
        container_.pop_front();
        put_condition_.notify_one();
        return true;
    }
    void Shutdown() {
        std::unique_lock<std::mutex> lock(mutex_);
        on_ = false;
        lock.unlock();
        put_condition_.notify_all();
        get_condition_.notify_all();
    }
};
