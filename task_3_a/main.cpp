#include <iostream>
#include <queue>
#include "solution.h"
#include <list>

int main() {
    int res;
    BlockingQueue<int, std::list<int>> bq(1);
    bq.Put(1);
    bq.Get(res);
    bq.Put(1);
    bq.Get(res);
    bq.Put(1);
    bq.Get(res);
    bq.Put(1);
//    bq.Shutdown();

    if (bq.Get(res)) {
        std::cout << res << std::endl;
    }
    else std::cout << "Get outta here!\n";
    //bq.Put(2);

    //bq.Get(res);

    return 0;

}
