#pragma once

#include <iostream>
#include <condition_variable>
#include <atomic>

template <class ConditionVariable = std::condition_variable>
class CyclicBarrier {
    size_t size;
    size_t capacity_;
    ConditionVariable condition_;
    std::mutex mutex;
    bool crossing_process_;

public:
    CyclicBarrier(std::size_t num_threads) {
        size = num_threads;
        capacity_ = num_threads;
        crossing_process_ = false;
    }

    void Pass() {
        std::unique_lock<std::mutex> lock(mutex);

        condition_.wait(lock, [this] { return !crossing_process_;});

        capacity_--;
        if (capacity_ == 0) {
            crossing_process_ = true;
            capacity_ = 1;
            condition_.notify_all();
        }
        else {
            condition_.wait(lock, [this]() { return crossing_process_;});
            capacity_++;
            if (capacity_ == size) {
                crossing_process_ = false;
                condition_.notify_all();
            }
        }
    }
};
