#include <iostream>
#include <thread>
#include <vector>
#include "solution.h"

int main() {
    CyclicBarrier<std::condition_variable> b(10);

    std::vector<std::thread> ts;
    for (int i = 0; i < 10; ++i) {
        ts.push_back(std::thread([&b]() {b.Pass();}));
    }

    for (auto &t : ts)
        t.join();

    return 0;
}
