#include <iostream>
#include "solution.h"

void function(const int &a) {
    std::cout << a << std::endl;
}

int main() {
    std::function<void(const int &)> func = function;
    std::vector<int> vector = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    hello_world(func, vector);
    return 0;
}