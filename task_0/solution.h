#pragma once

#include <vector>
#include <functional>
#include <thread>
#include <memory>

template <class Task>
void hello_world(std::function<void(const Task&)> func, const std::vector<Task>& tasks) {
    std::vector<std::thread> threads;
    for (auto& t : tasks) {
        threads.push_back(std::thread(func, t));
    }
    for (auto& t : threads) {
        t.join();
    }
}