#pragma once

#include <atomic>
#include <thread>
#include <array>

///////////////////////////////////////////////////////////////////////

template <typename T>
class LockFreeStack {
    struct Node {
        T element;
        Node* next;

        Node (const T& val): element(val) {
            next = nullptr;
        }
    };
    struct DeletedNode {
        Node* data;
        DeletedNode* next;

        DeletedNode(Node* val): data(val){}
    };




public:
    explicit LockFreeStack() {
    }

    ~LockFreeStack() {
        cleanDeletedList(deleted_top_);
        cleanStack(top_);
    }

    void Push(T element) {
        Node* new_top = new Node(element);
        Node* curr_top = top_.load();
        new_top->next = curr_top;
        while (!top_.compare_exchange_strong(curr_top, new_top)){
            new_top->next = curr_top;
        }
    }

    bool Pop(T& element) {
        Node* curr_top = top_;
        while (true) {
            if (!curr_top) {
                return false;
            }
            if (top_.compare_exchange_strong(curr_top, curr_top->next)) {
                element = curr_top->element;
                pushToDeletedList(curr_top);
                return true;
            }
        }
    }

    void cleanDeletedList(DeletedNode* nodes){
        while (nodes){
            DeletedNode* next = nodes->next;
            delete nodes->data;
            delete nodes;
            nodes = next;
        }
    }

    void cleanStack(Node* nodes){
        while (nodes){
            Node* next = nodes->next;
            delete nodes;
            nodes = next;
        }
    }


private:

    std::atomic<DeletedNode* > deleted_top_{nullptr};
    std::atomic<Node*> top_{nullptr};

    void pushToDeletedList(Node* items){
        DeletedNode* new_deleted_top = new DeletedNode(items);
        DeletedNode* curr_deleted_top = deleted_top_.load();
        new_deleted_top->next = curr_deleted_top;
        while (!deleted_top_.compare_exchange_strong(curr_deleted_top, new_deleted_top)){
            new_deleted_top->next = curr_deleted_top;
        }

    }


};

///////////////////////////////////////////////////////////////////////

template <typename T>
using ConcurrentStack = LockFreeStack<T>;

///////////////////////////////////////////////////////////////////////