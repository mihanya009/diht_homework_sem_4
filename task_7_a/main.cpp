#include <iostream>
#include "solution.h"

int main() {
    LockFreeStack<int> stack;
    stack.Push(1);
    //stack.Push(5);
    //stack.Push(4);
    int element;
    stack.Pop(element);
    //stack.Pop(element);
    std::cout << element << std::endl;
    return 0;
}