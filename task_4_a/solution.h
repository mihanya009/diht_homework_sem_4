#pragma once

#include <iostream>
#include <forward_list>
#include <vector>
#include <mutex>
#include <atomic>
#include <algorithm>
#include <shared_mutex>

template <typename T, class Hash = std::hash<T>>
class StripedHashSet {
    std::vector< std::forward_list<T> > buckets_;
    std::vector<std::shared_timed_mutex> stripes_;
    size_t growth_factor_;
    std::atomic<size_t> capacity_;
    double max_load_factor_;
    std::atomic<size_t> size_;
    Hash hash_;

    size_t GetStripeIndex(const size_t hash_value) const {
        return hash_value % stripes_.size();
    }

    size_t GetBucketIndex(const size_t hash_value) const {
        return hash_value % capacity_.load();
    }

    void resize() {
        stripes_[0].lock();
        if (double(size_.load()) / double(capacity_.load()) <= max_load_factor_) {
            stripes_[0].unlock();
            return;
        }
        for (auto lock = stripes_.begin() + 1; lock != stripes_.end(); ++lock) {
            lock->lock();
        }

        capacity_.store(capacity_.load() * growth_factor_);
        std::vector< std::forward_list<T> > new_buckets(capacity_.load());

        for (auto& bucket : buckets_) {
            for (auto& item : bucket) {
                size_t hash = hash_(item);
                size_t new_index = GetBucketIndex(hash);
                new_buckets[new_index].push_front(item);
            }
        }
        buckets_.swap(new_buckets);
        for (auto& lock : stripes_) {
            lock.unlock();
        }
    }

    bool unprotected_search_(size_t& bucket_index, const T& element) {
        auto it = std::find(buckets_[bucket_index].begin(),
                            buckets_[bucket_index].end(), element);
        return (it != buckets_[bucket_index].end());
    }

public:
    explicit StripedHashSet(const size_t concurrency_level,
                            const size_t growth_factor = 3,
                            const double max_load_factor = 0.75) {
        growth_factor_ = growth_factor;
        max_load_factor_ = max_load_factor;
        stripes_ = std::vector<std::shared_timed_mutex>(concurrency_level);
        buckets_ = std::vector< std::forward_list<T> >(concurrency_level);
        capacity_.store(concurrency_level);
        size_.store(0);
    }

    bool Insert(const T& element) {
        size_t hash = hash_(element);
        size_t stripe_index = GetStripeIndex(hash);
        std::unique_lock<std::shared_timed_mutex> lock(stripes_[stripe_index]);

        size_t bucket_index = GetBucketIndex(hash);
        if (unprotected_search_(bucket_index, element)) {
            return false;
        }
        else {
            buckets_[bucket_index].push_front(element);
            size_.fetch_add(1);
            lock.unlock();
            if (double(size_.load()) / double(capacity_.load()) > max_load_factor_) {
                resize();
            }
            return true;
        }
    }

    bool Remove(const T& element) {
        size_t hash = hash_(element);
        size_t stripe_index = GetStripeIndex(hash);
        std::shared_lock<std::shared_timed_mutex> lock(stripes_[stripe_index]);

        size_t bucket_index = GetBucketIndex(hash);
        if (unprotected_search_(bucket_index, element)) {
            buckets_[bucket_index].remove(element);
            size_.fetch_sub(1);
            return true;
        }
        else {
            return false;
        }
    }

    bool Contains(const T& element) {
        size_t hash = hash_(element);
        size_t stripe_index = GetStripeIndex(hash);
        std::unique_lock<std::shared_timed_mutex> lock(stripes_[stripe_index]);

        size_t bucket_index = GetBucketIndex(hash);
        return unprotected_search_(bucket_index, element);
    }

    size_t Size() {
        return size_.load();
    }
};

template <typename T> using ConcurrentSet = StripedHashSet<T>;
