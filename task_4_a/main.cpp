#include <iostream>
#include "solution.h"

int main() {
    StripedHashSet<int, std::hash<int>> set(10);
    for (int i = 0; i < 10000; i++) {
        set.Insert(i);
    }
    set.Insert(1);
    set.Insert(4);
    set.Insert(13);
    set.Remove(4);

    std::cout << set.Size() << std::endl;

    return 0;
}
