#include <thread>
#include "solution.h"

int main() {
    Robot robot;

    std::thread t1([&robot]() {for (int i = 0; i < 50; i++) robot.StepLeft(); });
    std::thread t2([&robot]() {for (int i = 0; i < 50; i++) robot.StepRight(); });

    t1.join();
    t2.join();
    return 0;
}
