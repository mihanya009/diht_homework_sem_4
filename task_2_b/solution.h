#pragma once

#include <iostream>
#include <condition_variable>
#include <atomic>

class Robot {
    std::condition_variable condition_;
    std::mutex mutex_;
    bool isLeft_;

public:
    Robot() {
        isLeft_ = true;
    }

    void StepLeft() {
        std::unique_lock<std::mutex> lock(mutex_);
        if (isLeft_) {
            isLeft_ = false;
            condition_.notify_one();
        }
        else {
            condition_.wait(lock, [this]() {return isLeft_; });
            isLeft_ = false;
            condition_.notify_one();
        }
        std::cout << "left" << std::endl;
    }

    void StepRight() {
        std::unique_lock<std::mutex> lock(mutex_);
        if (!isLeft_) {
            isLeft_ = true;
            condition_.notify_one();
        }
        else {
            condition_.wait(lock, [this]() {return !isLeft_; });
            isLeft_ = true;
            condition_.notify_one();
        }
        std::cout << "right" << std::endl;
    }
};
