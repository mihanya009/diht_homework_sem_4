#include <iostream>
#include "MutexTree.h"
#include <thread>

PetersonMutex::PetersonMutex() {
    want_[0].store(false);
    want_[1].store(false);
    victim_.store(0);
}

void PetersonMutex::lock(size_t thread_id) {
    want_[thread_id].store(true);
    victim_.store(thread_id);
    while (want_[1 - thread_id].load() && victim_.load() == thread_id) {
        // wait
        std::this_thread::yield();
    }
}

void PetersonMutex::unlock(size_t thread_id) {
    want_[thread_id].store(false);
}

MutexTree::MutexTree(size_t num_threads) {
    unsigned int k = 1;
    if (num_threads == 1) {
        k = 2;
    }
    else {
        while (k < num_threads) {
            k *= 2;
        }
    }

    tree_size_ = k - 1;
    tournaments_ = std::vector<PetersonMutex>(tree_size_);
}

void MutexTree::lock(size_t thread_index) {
    size_t current_index = tree_size_ + thread_index;
    bool is_right = current_index % 2 == 0;
    current_index = (current_index - 1) / 2;
    while (current_index > 0) {
        if (is_right) {
            tournaments_[current_index].lock(1);
        }
        else {
            tournaments_[current_index].lock(0);
        }
        is_right = current_index % 2 == 0;
        current_index = (current_index - 1) / 2;
    }
    if (is_right) {
        tournaments_[current_index].lock(1);
    }
    else {
        tournaments_[current_index].lock(0);
    }
}

void MutexTree::unlock(size_t thread_index) {
    size_t left = 0;
    size_t right = tree_size_ + 1;
    size_t current_index = 0;
    while (current_index != tree_size_ + thread_index) {
        if (thread_index >= (left + right) / 2) {
            tournaments_[current_index].unlock(1);
            left = (left + right) / 2;
            current_index = 2 * current_index + 2;
        }
        else {
            tournaments_[current_index].unlock(0);
            right = (left + right) / 2;
            current_index = 2 * current_index + 1;
        }
    }
}

