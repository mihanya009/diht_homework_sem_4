//
// Created by mikhail on 23.02.17.
//

#pragma once

#include <array>
#include <atomic>
#include <vector>
#include <memory>

class MutexTree;

class PetersonMutex {
    std::array<std::atomic<bool>, 2> want_;
    std::atomic<size_t> victim_;

public:
    PetersonMutex();
    void lock(size_t thread_id);
    void unlock(size_t thread_id);

    friend class MutexTree;
};

class MutexTree {
    std::vector<PetersonMutex> tournaments_;
    size_t tree_size_;

public:
    MutexTree(size_t num_threads);
    void lock(size_t thread_index);
    void unlock(size_t thread_index);
};

