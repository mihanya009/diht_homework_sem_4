#include <iostream>
#include "MutexTree.h"

int main() {
    MutexTree mutexTree(8);
    mutexTree.lock(1);
    mutexTree.unlock(1);
    mutexTree.lock(2);
    mutexTree.unlock(2);
}